﻿using UnityEngine;
using System.Collections;

public class PoisitionManager : MonoBehaviour {

    public Transform PlayerCameraTransform;
    public Transform[] PlayerPositions;

    public void Start()
    {
        ChangeCameraPosition(0);
    }

    public void ChangeCameraPosition(int target)
    {
        iTween.MoveTo(PlayerCameraTransform.gameObject, PlayerPositions[target].position, 5f * Time.deltaTime);
        iTween.RotateTo(PlayerCameraTransform.gameObject, PlayerPositions[target].localEulerAngles, 5f * Time.deltaTime);
    }
}
